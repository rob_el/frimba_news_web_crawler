package com.frimbaNews;

import com.frimbaNews.domain.Blog;
import com.frimbaNews.network.APIConnector;
import com.frimbaNews.network.Crawler;
import com.frimbaNews.network.RetrofitConnector;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        String key = "webdriver.chrome.driver";
        String value = "C:\\Users\\Robi\\IdeaProjects\\FrimbaNews\\Libs\\Chromedriver\\chromedriver.exe";
        System.setProperty(key,value);
        WebDriver driver = new ChromeDriver();

        Crawler etReporter = new Crawler.Builder(driver)
                .addTitleSelector(By.cssSelector("h3.post-title"))
                .addContentSelector(By.cssSelector("div.post-body"))
                .addDateSelector(By.cssSelector("span.post-created"))
                .addImageSelector(By.tagName("img"))
                .addNewsContainerSelector(By.cssSelector(".post-block.display-term.margin-bottom-30"))
                .setSourceWebsiteName("EtReporter")
                .setUrl("https://www.ethiopianreporter.com/index.php/poletika")
                .build();

        Crawler zehabesha = new Crawler.Builder(driver)
                .addTitleSelector(By.cssSelector("h2.posttitle"))
                .addContentSelector(By.tagName("p"))
                .addDateSelector(By.cssSelector("span.meta_date"))
                .addImageSelector(By.tagName("img"))
                .addNewsContainerSelector(By.cssSelector("div.featuredpost"))
                .setSourceWebsiteName("zeHabesha")
                .setUrl("http://www.zehabesha.com/amharic/")
                .build();


        List<Blog> etReporterList = etReporter.crawl();
        List<Blog> zehabeshaList = zehabesha.crawl();

        List<Blog> allBlog = Collections.emptyList();

        allBlog.addAll(etReporterList);
        allBlog.addAll(zehabeshaList);

        RetrofitConnector connector = APIConnector.getConnector();

        try {
            for (Blog blog : allBlog) {
                connector.insertNews(blog).execute();
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
