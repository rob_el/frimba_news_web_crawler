package com.frimbaNews.network;

import com.frimbaNews.domain.Blog;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface RetrofitConnector {

    @POST("blog/")
    Call<Void> insertNews(@Body Blog blog);

    @GET("blog/{site}/")
    Call<Blog> getLastNewsBySource(@Path("site") String site);
}
