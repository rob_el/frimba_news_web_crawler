package com.frimbaNews.domain;


public class Blog {

    private int blogId;
    private String blogTitle;
    private String blogContent;
    private String blogAuthor;
    private String blogImage;
    private String site;
    private String date;

    public Blog(int blogId,
                String blogTitle,
                String blogContent,
                String blogAuthor,
                String blogImage,
                String site,
                String date) {

        this.blogId = blogId;
        this.blogTitle = blogTitle;
        this.blogContent = blogContent;
        this.blogAuthor = blogAuthor;
        this.blogImage = blogImage;
        this.site = site;
        this.date = date;
    }

    public int getBlogId() {
        return blogId;
    }

    public void setBlogId(int id) {
        this.blogId = id;
    }

    public String getBlogTitle() {
        return blogTitle;
    }

    public void setBlogTitle(String title) {
        this.blogTitle = title;
    }

    public String getBlogContent() {
        return blogContent;
    }

    public void setBlogContent(String content) {
        this.blogContent = content;
    }

    public String getBlogAuthor() {
        return blogAuthor;
    }

    public void setBlogAuthor(String author) {
        this.blogAuthor = author;
    }

    public String getSite() {
        return this.site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getBlogImage() {
        return this.blogImage;
    }

    public void setBlogImage(String blogImage) {
        this.blogImage = blogImage;
    }

    @Override
    public String toString() {
        return "Blog:{" +
                "blogId='" + blogId + '\'' +
                ", blogTitle='" + blogTitle + '\'' +
                ", blogAuthor=" + blogAuthor +
                ", site='" + site + '\'' +
                ", blogContent='" + blogContent + '\'' +
                '}';
    }
}
