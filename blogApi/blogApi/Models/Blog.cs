﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace blogApi.Models
{
    public class Blog
    {
        public int blogId { get; set; }
        public string blogTitle { get; set; }
        public string blogAuthor { get; set; }
        public string blogContent { get; set; }
        public string blogImage { get; set; }
        public string date { get; set; }
        public string site { get; set; }
    }
}