namespace blogApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Blogs",
                c => new
                    {
                        blogId = c.Int(nullable: false, identity: true),
                        blogTitle = c.String(),
                        blogAuthor = c.String(),
                        blogContent = c.String(),
                        blogImage = c.String(),
                        date = c.DateTime(nullable: false),
                        site = c.String(),
                    })
                .PrimaryKey(t => t.blogId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Blogs");
        }
    }
}
