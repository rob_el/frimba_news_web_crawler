﻿using blogApi.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace blogApi
{
    public class blogApiDbContext:DbContext
    {
        public DbSet<Blog> Blog { get; set; }
    }
}