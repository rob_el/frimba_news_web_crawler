﻿using blogApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace blogApi.Controllers
{
    //[EnableCorsAttribute("*","*","*")]
    public class BlogController : ApiController
    {
        // GET: api/Blog
        blogApiDbContext context = new blogApiDbContext();

        public HttpResponseMessage Get()
        {
            try
            {
                List<Blog> result = context.Blog.ToList<Blog>();

                if (result.Count > 0)
                {
                    return Request.CreateResponse<List<Blog>>(HttpStatusCode.OK, result);

                }
                else
                {
                    return Request.CreateResponse<string>(HttpStatusCode.NotFound, "No results found!");
                }

            }
            catch (Exception e)
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, "There was an error! Error: " + e.Message);
            }
        }

        // GET: api/Blog/site
        public HttpResponseMessage Get(string site)
        {
            try
            {
                List<Blog> result = new List<Blog>();
                foreach(var blog in context.Blog.Where(b=> b.site == site))
                {
                    result.Add(blog);
                }

                if(result.Count > 0)
                {
                    return Request.CreateResponse<List<Blog>>(HttpStatusCode.OK, result);

                }
                else
                {
                    return Request.CreateResponse<string>(HttpStatusCode.NotFound, "No results found!");
                }

            }
            catch (Exception e)
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, "There was an error! Error: " + e.Message);
            }
        }

        public HttpResponseMessage Post([FromBody]Blog value)
        {
            try
            {
                context.Blog.Add(value);
                context.SaveChanges();

                return Request.CreateResponse<string>(HttpStatusCode.Created, "Blog is uploaded!");
            }
            catch(Exception e)
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, "There was an error! Error: " + e.Message);
            }
        }

    }
}
