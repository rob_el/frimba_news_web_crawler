import Vue from 'vue'
import App from './App.vue'
import VueResource from 'vue-resource'
import VueCookies from 'vue-cookies'
import VueChartjs from 'vue-chartjs'
import VueIcons from 'vue-icons'
import SuiVue from 'semantic-ui-vue';

Vue.use(VueCookies)
Vue.use(VueResource)
Vue.use(VueChartjs)
Vue.use(SuiVue)
Vue.use(VueIcons)

//filters

Vue.filter('addOne', function(value){
  return value + 1
})

Vue.filter('shorten', function(value){
    if(value !== null)
    {
        if(value.length > 50)
        {
            return value.substr(0, 50) + "..."
        }

        return value
    }
    else
    {
        return 0
    }

})

new Vue({
  el: '#app',
  render: h => h(App)
})
